from . import routes


# TODO: Add working thermostat API calls here.
# TODO: Make webhooks out of these API calls.

# Example API layout (not well-planned out. Should be changed).
# noinspection PyUnusedLocal
@routes.route('/api/temp/set-system', methods=['POST'])
def set_system_temperature(query_param):
    """
    Save system temperature passed through API query to the database.
    :param query_param: TODO: Temperature of the walls (obtained through some sort of sensor).
    :return: TODO: Send status 201 if properly appended to database (flask.Response class).
    """
    pass


# noinspection PyUnusedLocal
@routes.route('/api/temp/set-requested', methods=['POST'])
def set_ideal_temperature(query_param):
    """
    Assigns the requested temperature to database, and does isothermal calculations to get the
    real temperature needed, and returns that value to be used by the thermostat.
    :param query_param: TODO: Temperature the thermostat is set to.
    :return: TODO: Status 201 if properly appended to database (flask.Response class)
    """
    pass


@routes.route('/api/temp/update', methods=['GET'])
def update_temperature():
    """
    Called on a specific interval and whenever the temperature is manually changed. Returns the temperature the
    thermostat should be set to. This API call exists because the temperature of the walls and/or the desired
    temperature will change, and in order to handle that the temperature must be periodically updated.
    :return: TODO: Temperature the thermostat must be set to.
    """
    pass
