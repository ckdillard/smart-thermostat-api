import unittest
from bs4 import BeautifulSoup
from routing import home
from api import app


class HomeTest(unittest.TestCase):
    def test_home(self):
        with app.app_context():
            site = home()

            # Web scrape site to test that it is the proper page.
            page = BeautifulSoup(site, 'html.parser')
            if page.find('title', text='Isotherm API'):
                self.assertTrue(True)
            else:
                self.assertTrue(False)


