import unittest


class MyTestCase(unittest.TestCase):
    @unittest.skip('Not implemented.')
    def test_thermostat_sys_temp_set(self):
        """Checks that the database records the value correctly."""
        pass

    @unittest.skip('Not implemented.')
    def test_thermostat_ideal_temperature(self):
        """Checks that the set temperature records properly in the database."""
        pass

    @unittest.skip('Not implemented.')
    def test_thermostat_update_temperature(self):
        """Checks that the value to be sent to the thermostat is correct."""
        pass
