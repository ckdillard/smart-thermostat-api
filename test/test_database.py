import unittest
import os
from temperature_log import DataLogger


def reset():
    """Resets database file for accurate testing."""
    directory = os.path.dirname(os.path.realpath(__file__))
    parent = os.path.join(directory, os.pardir)
    logs_dir = os.path.join(parent, 'logs')
    test_db_file = os.path.join(logs_dir, 'test_log.db')

    if os.path.exists(test_db_file):
        os.remove(test_db_file)


class MyTestCase(unittest.TestCase):
    def setUp(self):
        reset()
        self.db = DataLogger('test')

    def test_execute_and_query(self):
        """Tests execute() method from DataLogger."""
        # Creates SQL information.
        sql = """INSERT INTO heating_records (t_out, t_nest, t_set)
                    VALUES(1, 2, 3)"""
        query = """SELECT * FROM heating_records"""

        # Execute SQL statements to push and pull info.
        self.db.execute(sql)
        path = self.db.get_db_path()
        info = self.db.query(query)

        self.assertEqual(info, [(1, 1, 2, 3)])
