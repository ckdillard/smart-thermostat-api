import os
import sqlite3


class DataLogger:
    def __init__(self, mode=None):
        # File path building for database file.
        self.__mode = mode
        self.__db = None
        self.__db_path = None
        self.__file_path = os.path.dirname(os.path.realpath(__file__))
        self.__root_path = os.path.join(self.__file_path, os.pardir)

        self.log_path = os.path.join(self.__root_path, 'logs')

        # Set up database file.
        self.__instantiate_database()

    def __instantiate_database(self):
        """Checks if database exists, and if not, creates a new connection in the logs folder."""
        # Creates the database record at proper location.
        if self.__mode == 'test':
            self.__db_path = os.path.realpath(os.path.join(self.log_path, 'test_log.db'))
        else:
            self.__db_path = os.path.realpath(os.path.join(self.log_path, 'log.db'))

        # Attempts to establish database connection.
        self.__open()

        try:
            # Creates proper tables if they don't exist already.
            sql = """
                  CREATE TABLE IF NOT EXISTS heating_records (
                    id     INTEGER PRIMARY KEY,
                    t_out  INT,
                    t_nest INT,
                    t_set  INT
                  )
                 """
            # c = self.__db.cursor()
            #
            # c.execute(sql)
            # self.__db.commit()
            # self.__close()
            self.execute(sql)

        except sqlite3.Error as err:
            print(err.args)

    def __open(self):
        """Establishes connection to file."""
        try:
            self.__db = sqlite3.connect(self.__db_path)

        except sqlite3.Error as err:
            print(err.args)

    def __close(self):
        """Closes file connection every time to minimize corruption."""
        try:
            self.__db.close()

        except sqlite3.Error as err:
            print(err.args)

    def get_db_path(self):
        """
        Allows access to database path directly. TESTING ONLY. NOT RECOMMENDED FOR USE.
        :return: database and path to database.
        """
        return self.__db_path

    def execute(self, sql):
        """
        Runs SQL code. Designed for inserts or table creations.
        :param: sql SQL encoded as string.
        """
        self.__open()
        c = self.__db.cursor()
        c.execute(sql)
        self.__db.commit()
        self.__close()

    def query(self, sql):
        """
        Queries database and returns values.
        :param sql SQL encoded as string.
        """
        self.__open()
        c = self.__db.cursor()
        var = c.execute(sql).fetchall()

        self.__close()
        return var
